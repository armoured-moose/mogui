import matplotlib.pyplot as plt
import scipy as sp

## This file is an example of how to unpack and plot the magnetic perturbation in rectangular coordinates,
## as saved in <working directory>/locust_input/bplas_rect.txt using the Mogui results tab.
## All that needs changing is the path_to_data variable

path_to_data = '/home/dryan/test_space_fuslw2/locust_input/bplas_rect.txt'


f = open(path_to_data, 'r')

line1 = f.readline()
line2 = f.readline()
line3 = f.readline()

f.close()

numR = eval(line3.split(',')[0].split('=')[-1])
numZ = eval(line3.split(',')[1].split('=')[-1])

data = sp.loadtxt(path_to_data)

# Unpack R and Z grid
R = data[:,0].reshape((numZ,numR))
Z = data[:,1].reshape((numZ,numR))

# Unpack Br,Bz,Bphi
BR = (data[:,2] + 1j*data[:,3]).reshape((numZ,numR))
BZ = (data[:,4] + 1j*data[:,5]).reshape((numZ,numR))
BP = (data[:,6] + 1j*data[:,7]).reshape((numZ,numR))

# Compute |B|
B = sp.sqrt(sp.absolute(BR)**2+sp.absolute(BZ)**2+sp.absolute(BP)**2)


## Plotting bit
plt.figure(figsize = (10,6))
plt.subplot(221)
plt.contourf(R,Z,BR.real*1e4,80, cmap = plt.cm.afmhot)
plt.title('Re{Br} G')
plt.colorbar()
plt.xlim([0,2.4])
plt.ylim([-1.4, 1.4])
plt.gca().set_aspect('equal', adjustable='box')

plt.subplot(222)
plt.contourf(R,Z,BZ.real*1e4,80, cmap = plt.cm.afmhot)
plt.title('Re{Bz} G')
plt.colorbar()
plt.xlim([0,2.4])
plt.ylim([-1.4, 1.4])
plt.gca().set_aspect('equal', adjustable='box')

plt.subplot(223)
plt.contourf(R,Z,BP.real*1e4,80, cmap = plt.cm.afmhot)
plt.title('Re{B$\phi$} G')
plt.colorbar()
plt.xlim([0,2.4])
plt.ylim([-1.4, 1.4])
plt.gca().set_aspect('equal', adjustable='box')

plt.subplot(224)
plt.contourf(R,Z,B*1e4,80, cmap = plt.cm.afmhot)
plt.title('|B| G')
plt.colorbar()
plt.xlim([0,2.4])
plt.ylim([-1.4, 1.4])
plt.gca().set_aspect('equal', adjustable='box')

plt.tight_layout()
plt.show()





