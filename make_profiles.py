import matplotlib.pyplot as plt
import scipy as sp
from scipy.interpolate import UnivariateSpline

general_data = sp.loadtxt('31021_019500_profiles.dat', skiprows=2)
vt_data = sp.loadtxt('31021_vtor_195000.dat', skiprows=1)

vt_original = vt_data[:,3] # m/s
s_vt_original = vt_data[:,1]
Rmaj_vt_original = vt_data[:,0]

wt_original = vt_original/Rmaj_vt_original #rad/s

s_gen_original = general_data[:,0]

ti_original = general_data[:,16] # eV
te_original = general_data[:,9] # eV
den_original = general_data[:,7] # pcls per m^3, *10^16


s_new = sp.linspace(0,1,500)

ti_new = UnivariateSpline(s_gen_original, ti_original,k=1)(s_new)
te_new = UnivariateSpline(s_gen_original, te_original,k=1)(s_new)
den_new = UnivariateSpline(s_gen_original, den_original,k=1)(s_new)

wt_new = UnivariateSpline(s_vt_original, wt_original)(s_new)


plt.subplot(311)
plt.plot(s_new, ti_new*1e-3, s_new, te_new*1e-3)
plt.plot(s_gen_original, ti_original*1e-3, 'ks', markersize=4.0)
plt.plot(s_gen_original, te_original*1e-3, 'ko', markersize=4.0)
plt.legend(['Ti','Te'])
plt.ylabel('T$_{i,e}$, keV', fontsize=16)
plt.xlabel('s', fontsize=16)

plt.subplot(312)
plt.plot(s_vt_original, wt_original*1e-3, 'ko', markevery=30)
plt.plot(s_new, wt_new*1e-3)
plt.ylabel('$\Omega_T$ krad/s', fontsize=16)
plt.xlabel('s', fontsize=16)


plt.subplot(313)
plt.plot(s_gen_original, den_original*1e-3, 'ko')
plt.plot(s_new, den_new*1e-3)
plt.ylabel('$n_e$, 1e19 m$^{-3}$', fontsize=16)
plt.xlabel('s', fontsize=16)
plt.show()


sp.savetxt('PROFTI_31021_1950.IN',sp.transpose([s_new,ti_new]), header='500 1',comments='') # save Ti in eV
sp.savetxt('PROFTE_31021_1950.IN',sp.transpose([s_new,te_new]), header='500 1',comments='') # save Te in eV
sp.savetxt('PROFROT_31021_1950.IN',sp.transpose([s_new,wt_new]), header='500 1',comments='') # save rotation in rad/s
sp.savetxt('PROFWE_31021_1950.IN',sp.transpose([s_new,wt_new]), header='500 1',comments='') # save rotation in rad/s
sp.savetxt('PROFDEN_31021_1950.IN',sp.transpose([s_new,den_new*1e16]), header='500 1',comments='')


"""
import matplotlib.pyplot as plt
import scipy as sp
from scipy.interpolate import UnivariateSpline


if False: # new profiles for 2250ms
  general_data = sp.loadtxt('31021_022500_profiles.dat', skiprows=2)
  vt_data = sp.loadtxt('31021_vtor_225000.dat', skiprows=1)
  
  vt_original = vt_data[:,3] # m/s
  s_vt_original = vt_data[:,1]
  Rmaj_vt_original = vt_data[:,0]

  wt_original = vt_original/Rmaj_vt_original #rad/s
  
  s_gen_original = general_data[:,0]
  
  ti_original = general_data[:,16] # eV
  te_original = general_data[:,9] # eV
  den_original = general_data[:,7] # pcls per m^3, *10^16


  s_new = sp.linspace(0,1,500)

  ti_new = UnivariateSpline(s_gen_original, ti_original)(s_new)
  te_new = UnivariateSpline(s_gen_original, te_original)(s_new)
  den_new = UnivariateSpline(s_gen_original, den_original)(s_new)

  wt_new = UnivariateSpline(s_vt_original, wt_original)(s_new)


  plt.figure(1)
  plt.plot(s_new, ti_new*1e-3, s_new, te_new*1e-3)
  plt.plot(s_gen_original, ti_original*1e-3, 'ks', markersize=4.0)
  plt.plot(s_gen_original, te_original*1e-3, 'ko', markersize=4.0)
  plt.legend(['Ti','Te'])
  plt.ylabel('T$_{i,e}$, keV', fontsize=16)
  plt.xlabel('s', fontsize=16)

  plt.figure(2)
  plt.plot(s_vt_original, wt_original*1e-3, 'ko', markevery=30)
  plt.plot(s_new, wt_new*1e-3)
  plt.ylabel('$\Omega_T$ krad/s', fontsize=16)
  plt.xlabel('s', fontsize=16)


  plt.figure(3)
  plt.plot(s_gen_original, den_original*1e-3, 'ko')
  plt.plot(s_new, den_new*1e-3)
  plt.ylabel('$n_e$, 1e19 m$^{-3}$', fontsize=16)
  plt.xlabel('s', fontsize=16)
  plt.show()

  #sp.savetxt('PROFTI_31021_2250.IN',sp.transpose([s_new,ti_new]), header='500 1',comments='') # save Ti in eV
  #sp.savetxt('PROFTE_31021_2250.IN',sp.transpose([s_new,te_new]), header='500 1',comments='') # save Te in eV
  #sp.savetxt('PROFROT_31021_2250.IN',sp.transpose([s_new,wt_new]), header='500 1',comments='') # save rotation in rad/s
  #sp.savetxt('PROFWE_31021_2250.IN',sp.transpose([s_new,wt_new]), header='500 1',comments='') # save rotation in rad/s
  #sp.savetxt('PROFDEN_31021_2250.IN',sp.transpose([s_new,den_new*1e16]), header='500 1',comments='')






if True: # old profiles for 2300ms
  # File containing rho (=s) and major radius Rmaj
  data = sp.loadtxt('rho_rmaj.txt')
  Rmaj = data[:,0]
  rho1 = data[:,1]

  data2 = sp.loadtxt('vtor_ti_31021_3200ms_fit.txt', skiprows=2)
  data3 = sp.loadtxt('ne_data.txt', skiprows=1)
  data4 = sp.loadtxt('te_data.txt', skiprows=1)


  te_rho = data4[:,1]
  Te = data4[:,3]
   
  ne_rho = data3[:,1]
  ne = data3[:,3]

  rho2 = data2[:,0]
  Vtor = data2[:,1] # in m/s
  Ti = data2[:,2]




  new_Rmaj = UnivariateSpline(rho1, Rmaj,s=0)(rho2)

  Wt = sp.divide(Vtor,new_Rmaj)
   

  rho3 = sp.linspace(0,1,500)

  Ti_hires = UnivariateSpline(rho2, Ti,s=0)(rho3)
  Te_hires = UnivariateSpline(te_rho, Te,s=0)(rho3)



  Wt_hires = UnivariateSpline(rho2, Wt,s=0)(rho3)
  ne_hires = UnivariateSpline(ne_rho, ne,s=0)(rho3)

#  plt.figure(1)
#  plt.plot(rho3, Ti_hires, rho3, Te_hires*1e-3)
#  plt.plot(rho2, Ti, 'ks', markersize=4.0, markevery=5)
#  plt.plot(te_rho, Te*1e-3, 'ko', markersize=4.0, markevery=50)
#  plt.legend(['Ti','Te'])
#  plt.ylabel('T$_{i,e}$, keV', fontsize=16)
#  plt.xlabel('s', fontsize=16)

  plt.figure(2)
  plt.plot(rho2, Wt*100, 'ko', markevery=10)
  plt.plot(rho3, Wt_hires*100)
  plt.ylabel('$\Omega_T$ krad/s', fontsize=16)
  plt.xlabel('s', fontsize=16)


#  plt.figure(3)
#  plt.plot(ne_rho, ne*1e-19, 'ko', markevery=30)
#  plt.plot(rho3, ne_hires*1e-19)
#  plt.ylabel('$n_e$, 1e19 m$^{-3}$', fontsize=16)
#  plt.xlabel('s', fontsize=16)
  plt.show()



  #sp.savetxt('PROFTI.IN',sp.transpose([rho3,Ti_hires*1e3]), header='500 1',comments='') # save Ti in eV
  #sp.savetxt('PROFTE.IN',sp.transpose([rho3,Te_hires]), header='500 1',comments='') # save Te in eV
  #sp.savetxt('PROFROT.IN',sp.transpose([rho3,Wt_hires*1e5]), header='500 1',comments='') # save rotation in rad/s
  #sp.savetxt('PROFWE.IN',sp.transpose([rho3,Wt_hires*1e5]), header='500 1',comments='') # save rotation in rad/s
  #sp.savetxt('PROFDEN.IN',sp.transpose([rho3,ne_hires]), header='500 1',comments='')
"""


