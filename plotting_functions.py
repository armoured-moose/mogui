import scipy as sp
import scipy.interpolate
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt
import plotting_base as pb
import os

def plotMagSpect_1coil(bplas_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, plotbench, b1_or_bn, plot_rational, B0EXP):
  nchi=660
  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  rz_pest = pb.rzcoords(rmzm_pest_path, nchi)
  jc_pest = pb.jacobian(rz_pest)
  bplas_geom = pb.bplasma(bplas_path, rz_geom,jc_geom)
  bn_geom = sp.copy(bplas_geom.bn)
  bn_pest = scipy.interpolate.griddata((rz_geom.R.ravel(), rz_geom.Z.ravel()), bn_geom.ravel(), (rz_pest.R, rz_pest.Z))
  b1_pest = bn_pest*sp.sqrt(jc_pest.G22*jc_pest.G33)
  expmchi = sp.exp(sp.tensordot(rz_pest.chi,bplas_geom.m, 0)*-1j)
  bm1_pest = sp.dot(sp.dot(b1_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi
  bm1_pest = sp.array(bm1_pest)
  profeq = sp.loadtxt(profeq_path)
  s_pest = profeq[:,0] 
  q_pest = profeq[:,1] 
  dpsi_pest = profeq[:,11]
  dpsi_pest[0] = dpsi_pest[1] 
  q_max = q_pest.max()
  q_min = q_pest.min()
  m_rational = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
  q_rational = m_rational/float(n)
  s_rational = InterpolatedUnivariateSpline(q_pest, s_pest)(q_rational)
  res = sp.absolute(bm1_pest[:rz_pest.Ns_plas,:])
  res_rational=[]
  s_rational_closest = []
  for m_index, m in enumerate(m_rational):
      res_rational.append(sp.absolute(bm1_pest[sp.absolute(rz_pest.s - s_rational[m_index]).argmin(),sp.where(sp.array(bplas_geom.m)[:]==m)[0][0]]))
      s_rational_closest.append(rz_pest.s[sp.absolute(rz_pest.s - s_rational[m_index]).argmin()])
  
  if plotbench==0:
    if b1_or_bn==0:
      if plot_rational==0:
        plt.figure('Magnetic Spectrogram (dimensionless), single MARS run')
        plt.contourf(sp.array(bplas_geom.m)[:],rz_geom.s[:rz_pest.Ns_plas], res*1e4, 80, cmap=plt.cm.spectral_r)
        plt.plot(m_rational[:len(s_rational_closest)], s_rational_closest,'+w', markersize=6.0, markeredgewidth=1.5)
        plt.colorbar()
        plt.title('$b^1 \\times 10^4$, MARS unit', fontsize=20)
        plt.xlabel('m', fontsize=18)
        plt.ylabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)
      elif plot_rational==1:
        plt.figure('b1 (MARS units) at rational surfaces, single MARS run')
        plt.plot(s_rational_closest, sp.array(res_rational)*1e4, '-o')
        plt.xlabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)
        plt.ylabel('$b^1_{res} \\times 10^4$, MARS unit', fontsize=20)
    elif b1_or_bn==1:
      bmn_pest = sp.dot(sp.dot(bn_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi
      bmn_pest = sp.array(bmn_pest)
      res = sp.absolute(bmn_pest[:rz_pest.Ns_plas,:])#*1e4
      plt.figure('Magnetic Spectrogram (G), single MARS run')
      plt.contourf(sp.array(bplas_geom.m)[:],rz_geom.s[:rz_pest.Ns_plas], res*1e4*B0EXP, 80, cmap=plt.cm.spectral_r)
      plt.plot(m_rational, s_rational_closest,'+w', markersize=6.0, markeredgewidth=1.5)
      plt.colorbar()
      plt.title('|$b^n_m$| (G)', fontsize=20)
      plt.xlabel('m', fontsize=18)
      plt.ylabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)

  if plotbench==1:
    G22 = jc_pest.G22[0:rz_pest.Ns_plas,:]
    G11U = G22*(rz_pest.R[0:rz_pest.Ns_plas,:]/jc_pest.jacobian[0:rz_pest.Ns_plas,:])**2
    grads = sp.sum(sp.sqrt(G11U),1)/G11U.shape[1]
    grads[0]=grads[1]
    res_ergos = sp.zeros(sp.shape(res))

    for i in range(len(sp.array(bplas_geom.m)[:])):
      res_ergos[:,i] = res[:,i]*2.0/(q_pest*dpsi_pest)

    res_rational = sp.array(res_rational)
    dpsi_rational = UnivariateSpline(s_pest,dpsi_pest,s=0)(s_rational)
    grads_rational  = UnivariateSpline(s_pest,grads,s=0)(s_rational)
    benchmark = res_rational*2.0/(q_rational*dpsi_rational*grads_rational)
    plt.figure('Magnetic Spectrogram, single MARS run', figsize=(14,6))
    plt.subplot(121)
    plt.contourf(sp.array(bplas_geom.m)[:],rz_geom.s[:rz_pest.Ns_plas], res_ergos*1e4, 30, cmap=plt.cm.CMRmap)
    plt.plot(m_rational[:len(s_rational_closest)], s_rational_closest,'+b', markersize=6.0, markeredgewidth=1.5)
    plt.colorbar()
    plt.title('$b^1 \\times 10^4$, ERGOS unit', fontsize=20)
    plt.xlabel('m', fontsize=18)
    plt.ylabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)
    plt.subplot(122)
    plt.plot(s_rational_closest, benchmark*1e4,'-s', markersize=6.0, markeredgewidth=1.5)
    plt.ylim(ymin=0.0)
    plt.xlabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)
    plt.ylabel('$b^r_{res} \\times 10^4$, ERGOS unit', fontsize=18)
    plt.title('$b^r_{res} \\times 10^4$, ERGOS unit', fontsize=20)

  plt.show(block=False)

def plotBplasProfiles_1coil(bplas_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, min_harmonic, max_harmonic, b1_or_bn, B0EXP):
  nchi=660
  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  rz_pest = pb.rzcoords(rmzm_pest_path, nchi)
  jc_pest = pb.jacobian(rz_pest)
  bplas_geom = pb.bplasma(bplas_path, rz_geom,jc_geom)
  bn_geom = sp.copy(bplas_geom.bn)
  bn_pest = scipy.interpolate.griddata((rz_geom.R.ravel(), rz_geom.Z.ravel()), bn_geom.ravel(), (rz_pest.R, rz_pest.Z))
  b1_pest = bn_pest*sp.sqrt(jc_pest.G22*jc_pest.G33)
  expmchi = sp.exp(sp.tensordot(rz_pest.chi,bplas_geom.m, 0)*-1j)
  bm1_pest = sp.dot(sp.dot(b1_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi
  bm1_pest = sp.array(bm1_pest)
  profeq = sp.loadtxt(profeq_path)
  s_pest = profeq[:,0] 
  q_pest = profeq[:,1] 
  dpsi_pest = profeq[:,11]
  dpsi_pest[0] = dpsi_pest[1] 
  q_max = q_pest.max()
  q_min = q_pest.min()
  m_rational = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
  q_rational = m_rational/float(n)
  s_rational = InterpolatedUnivariateSpline(q_pest, s_pest)(q_rational)
  if b1_or_bn==0: # to plot b1
    res = sp.absolute(bm1_pest[:rz_pest.Ns_plas,:])*1e4
    plt.figure('Magnetic Profiles b1m, single MARS run')
  elif b1_or_bn==1: # to plot bn in Gauss 
    bmn_pest = sp.dot(sp.dot(bn_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi
    bmn_pest = sp.array(bmn_pest)
    res = sp.absolute(bm1_pest[:rz_pest.Ns_plas,:])*1e4*B0EXP
    plt.figure('Magnetic Profiles bnm, single MARS run')
  for i in range(min_harmonic, max_harmonic+1):
    plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,sp.where(bplas_geom.m==i)[0][0]])
  plt.legend(['m = ' + str(x) for x in range(min_harmonic, max_harmonic+1)],loc=2)

  plt.show(block=False)
  

def plotBplasRZ_1coil(bplas_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, r0, b0):
  nchi=660
  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  bplas_geom = pb.bplasma(bplas_path, rz_geom,jc_geom)
  B = bplas_geom.AbsB*b0
  BN = bplas_geom.bn*b0
  BR = bplas_geom.Br*b0
  BZ = bplas_geom.Bz*b0
  BP = bplas_geom.Bphi*b0
  R = rz_geom.R*r0
  Z = rz_geom.Z*r0
  # set max value to suppress singularities near R=0
  absMax = B[sp.where(R>1.0)].max()*1e4
  brMax = BR.real[sp.where(R>0.8)].max()*1e4
  bzMax = BZ.real[sp.where(R>0.8)].max()*1e4
  bpMax = BP.real[sp.where(R>0.8)].max()*1e4

  brMin = BR.real[sp.where(R>0.8)].min()*1e4
  bzMin = BZ.real[sp.where(R>0.8)].min()*1e4
  bpMin = BP.real[sp.where(R>0.8)].min()*1e4

  plt.figure('Magnetic Perturbation B(R,Z), single MARS run',figsize = (10,6))
  plt.subplot(231)
  plt.contourf(R,Z,BR.real*1e4,80, cmap = plt.cm.afmhot, vmin = brMin, vmax = brMax, levels = sp.linspace(brMin,brMax,100))
  plt.title('Re{Br} G')
  plt.colorbar(format = '%0.1f')
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(232)
  plt.contourf(R,Z,BZ.real*1e4,80, cmap = plt.cm.afmhot, vmin = bzMin, vmax = bzMax, levels = sp.linspace(bzMin,bzMax,100))
  plt.title('Re{Bz} G')
  plt.colorbar(format = '%0.1f')
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(233)
  plt.contourf(R,Z,BP.real*1e4,80, cmap = plt.cm.afmhot, vmin = bpMin, vmax = bpMax, levels = sp.linspace(bpMin,bpMax,100))
  plt.title('Re{B$\phi$} G')
  plt.colorbar(format = '%0.1f')
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(234)
  plt.contourf(R,Z,B*1e4,80, cmap = plt.cm.afmhot, vmax = absMax, levels = sp.linspace(0,absMax,100))
  plt.title('|B| G')
  plt.colorbar(format = '%0.1f')
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(235)
  plt.contourf(R,Z,BN.real*1e4,80, cmap = plt.cm.afmhot)
  plt.title('Re{Bn} G')
  plt.colorbar()
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(236)
  plt.contourf(R,Z,BN.imag*1e4,80, cmap = plt.cm.afmhot)
  plt.title('Im{Bn} G')
  plt.colorbar()
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')
  plt.tight_layout()
  plt.show(block=False)


def plotMagSpect_2coil(bplas_u_path, bplas_l_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, deltaphi, plotbench, b1_or_bn, plot_rational, B0EXP): 
  deltaphi_rad = deltaphi*(sp.pi/180.0) 
  nchi=660
  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  rz_pest = pb.rzcoords(rmzm_pest_path, nchi)
  jc_pest = pb.jacobian(rz_pest)
  bplas_u_geom = pb.bplasma(bplas_u_path, rz_geom,jc_geom)
  bm1_u_geom = sp.copy(bplas_u_geom.bm1)
  bplas_l_geom = pb.bplasma(bplas_l_path, rz_geom,jc_geom)
  bm1_l_geom = sp.copy(bplas_l_geom.bm1)
  bm1_total = bm1_u_geom + bm1_l_geom*sp.exp(-1j*deltaphi_rad)
  sp.reshape(bm1_total, (bplas_u_geom.Nm1,rz_geom.Ns))
  expmchi = sp.exp(sp.tensordot(bplas_u_geom.m, rz_geom.chi,0)*1j)
  b1_total = sp.dot(bm1_total.T, expmchi)
  bn_total = b1_total/sp.sqrt(jc_geom.G22*jc_geom.G33)
  b1_total = sp.array(b1_total)
  bn_total = sp.array(bn_total)
  bn_pest = scipy.interpolate.griddata((rz_geom.R.ravel(), rz_geom.Z.ravel()), bn_total.ravel(), (rz_pest.R, rz_pest.Z))
  b1_pest = bn_pest*sp.sqrt(jc_pest.G22*jc_pest.G33)
  expmchi = sp.exp(sp.tensordot(rz_pest.chi,bplas_u_geom.m, 0)*-1j)
  bm1_pest = sp.dot(sp.dot(b1_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi
  bm1_pest = sp.array(bm1_pest)

  # Loading qprof to get fac_ERGOS, and also rational surfaces
  profeq = sp.loadtxt(profeq_path)
  s_pest = profeq[:,0] 
  q_pest = profeq[:,1] 
  dpsi_pest = profeq[:,11]
  dpsi_pest[0]=dpsi_pest[1]

  # get rational surfaces
  q_max = q_pest.max()
  q_min = q_pest.min()
  m_rational = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
  q_rational = m_rational/float(n)
  s_rational = InterpolatedUnivariateSpline(q_pest, s_pest)(q_rational)
  res = sp.absolute(bm1_pest[:rz_pest.Ns_plas,:])#*1e4
  res_rational=[]
  s_rational_closest = []
  for m_index, m in enumerate(m_rational):
      res_rational.append(sp.absolute(bm1_pest[sp.absolute(rz_pest.s - s_rational[m_index]).argmin(),sp.where(sp.array(bplas_u_geom.m)[:]==m)[0][0]]))
      s_rational_closest.append(rz_pest.s[sp.absolute(rz_pest.s - s_rational[m_index]).argmin()])

  if plotbench==0:
    if b1_or_bn==0:
      if plot_rational==0:
        plt.figure('Magnetic Spectrogram, two MARS runs')
        plt.contourf(sp.array(bplas_u_geom.m)[:],rz_geom.s[:rz_pest.Ns_plas], res*1e4, 80, cmap=plt.cm.spectral_r)
        plt.plot(m_rational, s_rational_closest,'+w', markersize=6.0, markeredgewidth=1.5)
        plt.colorbar()
        plt.title('$b^1_m \\times 10^4$', fontsize=20)
        plt.xlabel('m', fontsize=18)
        plt.ylabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)
      elif plot_rational==1:
        plt.figure('b1 (MARS units) at rational surfaces, single MARS run')
        plt.plot(s_rational_closest, sp.array(res_rational)*1e4, '-o')
        plt.xlabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)
        plt.ylabel('$b^1_{res} \\times 10^4$, MARS unit', fontsize=20)
    elif b1_or_bn==1:
      bmn_pest = sp.dot(sp.dot(bn_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi
      bmn_pest = sp.array(bmn_pest)
      res = sp.absolute(bmn_pest[:rz_pest.Ns_plas,:])
      plt.figure('b^n_m Magnetic Spectrogram (G), two MARS runs')
      plt.contourf(sp.array(bplas_u_geom.m)[:],rz_geom.s[:rz_pest.Ns_plas], res*1e4*B0EXP, 80, cmap=plt.cm.spectral_r)
      plt.plot(m_rational, s_rational_closest,'+w', markersize=6.0, markeredgewidth=1.5)
      plt.colorbar()
      plt.title('|$b^n_m$| (G)', fontsize=20)
      plt.xlabel('m', fontsize=18)
      plt.ylabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)

  if plotbench==1:
    G22 = jc_pest.G22[0:rz_pest.Ns_plas,:]
    G11U = G22*(rz_pest.R[0:rz_pest.Ns_plas,:]/jc_pest.jacobian[0:rz_pest.Ns_plas,:])**2
    grads = sp.sum(sp.sqrt(G11U),1)/G11U.shape[1]
    grads[0]=grads[1]
    res_ergos = sp.zeros(sp.shape(res))

    for i in range(len(sp.array(bplas_u_geom.m)[:])):
      res_ergos[:,i] = res[:,i]*2.0/(q_pest*dpsi_pest)

    res_rational = sp.array(res_rational)
    dpsi_rational = UnivariateSpline(s_pest,dpsi_pest,s=0)(s_rational)
    grads_rational  = UnivariateSpline(s_pest,grads,s=0)(s_rational)
    benchmark = res_rational*2.0/(q_rational*dpsi_rational*grads_rational)
    plt.figure('Magnetic Spectrogram, two MARS runs', figsize=(14,6))
    plt.subplot(121)
    plt.contourf(sp.array(bplas_u_geom.m)[:],rz_geom.s[:rz_pest.Ns_plas], res_ergos*1e4, 30, cmap=plt.cm.CMRmap)
    plt.plot(m_rational, s_rational_closest,'+b', markersize=6.0, markeredgewidth=1.5)
    plt.colorbar()
    plt.title('$b^1 \\times 10^4$, ERGOS unit', fontsize=20)
    plt.xlabel('m', fontsize=18)
    plt.ylabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)
    plt.subplot(122)
    plt.plot(s_rational_closest, benchmark*1e4,'-s', markersize=6.0, markeredgewidth=1.5)
    plt.ylim(ymin=0.0)
    plt.xlabel('s=$\sqrt{\psi_{pol}}$', fontsize=18)
    plt.ylabel('$b^r_{res} \\times 10^4$, ERGOS unit', fontsize=18)
    plt.title('$b^r_{res} \\times 10^4$, ERGOS unit', fontsize=20)

  plt.show(block=False)
  
def plotBplasProfiles_2coil(bplas_u_path, bplas_l_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, deltaphi, min_harmonic, max_harmonic, b1_or_bn, B0EXP): 
  deltaphi_rad = deltaphi*(sp.pi/180.0) 
  nchi=660
  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  rz_pest = pb.rzcoords(rmzm_pest_path, nchi)
  jc_pest = pb.jacobian(rz_pest)
  bplas_u_geom = pb.bplasma(bplas_u_path, rz_geom,jc_geom)
  bm1_u_geom = sp.copy(bplas_u_geom.bm1)
  bplas_l_geom = pb.bplasma(bplas_l_path, rz_geom,jc_geom)
  bm1_l_geom = sp.copy(bplas_l_geom.bm1)
  bm1_total = bm1_u_geom + bm1_l_geom*sp.exp(-1j*deltaphi_rad)
  sp.reshape(bm1_total, (bplas_u_geom.Nm1,rz_geom.Ns))
  expmchi = sp.exp(sp.tensordot(bplas_u_geom.m, rz_geom.chi,0)*1j)
  b1_total = sp.dot(bm1_total.T, expmchi)
  bn_total = b1_total/sp.sqrt(jc_geom.G22*jc_geom.G33)
  b1_total = sp.array(b1_total)
  bn_total = sp.array(bn_total)
  bn_pest = scipy.interpolate.griddata((rz_geom.R.ravel(), rz_geom.Z.ravel()), bn_total.ravel(), (rz_pest.R, rz_pest.Z))
  b1_pest = bn_pest*sp.sqrt(jc_pest.G22*jc_pest.G33)
  expmchi = sp.exp(sp.tensordot(rz_pest.chi,bplas_u_geom.m, 0)*-1j)
  bm1_pest = sp.dot(sp.dot(b1_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi
  bm1_pest = sp.array(bm1_pest)

  # Loading qprof to get fac_ERGOS, and also rational surfaces
  profeq = sp.loadtxt(profeq_path)
  s_pest = profeq[:,0] 
  q_pest = profeq[:,1] 
  dpsi_pest = profeq[:,11]
  dpsi_pest[0]=dpsi_pest[1]

  # get rational surfaces
  q_max = q_pest.max()
  q_min = q_pest.min()
  m_rational = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
  q_rational = m_rational/float(n)
  s_rational = InterpolatedUnivariateSpline(q_pest, s_pest)(q_rational)
  if b1_or_bn==0: # to plot b1
    res = sp.absolute(bm1_pest[:rz_pest.Ns_plas,:])*1e4
    plt.figure('Magnetic Profiles b1m, two MARS runs')
  elif b1_or_bn==1: # to plot bn in Gauss 
    bmn_pest = sp.dot(sp.dot(bn_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi
    bmn_pest = sp.array(bmn_pest)
    res = sp.absolute(bm1_pest[:rz_pest.Ns_plas,:])*1e4*B0EXP
    plt.figure('Magnetic Profiles bnm, two MARS runs')
  for i in range(min_harmonic, max_harmonic+1):
    plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,sp.where(bplas_u_geom.m==i)[0][0]])
  plt.legend(['m = ' + str(x) for x in range(min_harmonic, max_harmonic+1)],loc=2)

  plt.show(block=False)
  
def plotBplasRZ_2coil(bplas_u_path, bplas_l_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, r0, b0, deltaphi): 
  deltaphi_rad = deltaphi*(sp.pi/180.0)

  nchi=660
  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  bplas_u_geom = pb.bplasma(bplas_u_path, rz_geom,jc_geom)
  bplas_l_geom = pb.bplasma(bplas_l_path, rz_geom,jc_geom)
  BN = (bplas_u_geom.bn + bplas_l_geom.bn*sp.exp(-1j*deltaphi_rad))*b0
  BR = (bplas_u_geom.Br + bplas_l_geom.Br*sp.exp(-1j*deltaphi_rad))*b0
  BZ = (bplas_u_geom.Bz + bplas_l_geom.Bz*sp.exp(-1j*deltaphi_rad))*b0
  BP = (bplas_u_geom.Bphi + bplas_l_geom.Bphi*sp.exp(-1j*deltaphi_rad))*b0
  B = sp.sqrt(sp.square(sp.absolute(BR)) + sp.square(sp.absolute(BZ)) + sp.square(sp.absolute(BP)))
  R = rz_geom.R*r0
  Z = rz_geom.Z*r0


  absMax = B[sp.where(R>1.0)].max()*1e4
  brMax = BR.real[sp.where(R>0.8)].max()*1e4
  bzMax = BZ.real[sp.where(R>0.8)].max()*1e4
  bpMax = BP.real[sp.where(R>0.8)].max()*1e4

  brMin = BR.real[sp.where(R>0.8)].min()*1e4
  bzMin = BZ.real[sp.where(R>0.8)].min()*1e4
  bpMin = BP.real[sp.where(R>0.8)].min()*1e4


  plt.figure('Magnetic Perturbation B(R,Z), two MARS runs', figsize = (10,6))
  plt.subplot(231)
  plt.contourf(R,Z,BR.real*1e4,80, cmap = plt.cm.afmhot, vmin = brMin, vmax = brMax, levels = sp.linspace(brMin,brMax,100))
  plt.title('Re{Br} G')
  plt.colorbar(format = '%0.1f')
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(232)
  plt.contourf(R,Z,BZ.real*1e4,80, cmap = plt.cm.afmhot, vmin = bzMin, vmax = bzMax, levels = sp.linspace(bzMin,bzMax,100))
  plt.title('Re{Bz} G')
  plt.colorbar(format = '%0.1f')
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(233)
  plt.contourf(R,Z,BP.real*1e4,80, cmap = plt.cm.afmhot, vmin = bpMin, vmax = bpMax, levels = sp.linspace(bpMin,bpMax,100))
  plt.title('Re{B$\phi$} G')
  plt.colorbar(format = '%0.1f')
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(234)
  plt.contourf(R,Z,B*1e4,80, cmap = plt.cm.afmhot, vmax = absMax, levels = sp.linspace(0,absMax,100))
  plt.title('|B| G')
  plt.colorbar(format = '%0.1f')
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(235)
  plt.contourf(R,Z,BN.real*1e4,80, cmap = plt.cm.afmhot)
  plt.title('Re{Bn} G')
  plt.colorbar()
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(236)
  plt.contourf(R,Z,BN.imag*1e4,80, cmap = plt.cm.afmhot)
  plt.title('Im{Bn} G')
  plt.colorbar()
  plt.xlim([0,2.4])
  plt.ylim([-1.4, 1.4])
  plt.gca().set_aspect('equal', adjustable='box')
  plt.tight_layout()
  plt.show(block=False)


def plotDispSpect_1coil(xplas_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n):

  nchi=660

  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  rz_pest = pb.rzcoords(rmzm_pest_path, nchi)
  jc_pest = pb.jacobian(rz_pest)

  xplas = pb.xplasma(xplas_path, rz_geom, jc_geom)
  
  xn_geom = sp.copy(xplas.xn)

  xn_pest = scipy.interpolate.griddata((rz_geom.R[:int(rz_geom.Ns_plas),:].ravel(), rz_geom.Z[:int(rz_geom.Ns_plas),:].ravel()), xn_geom.ravel(), (rz_pest.R[:int(rz_geom.Ns_plas),:], rz_pest.Z[:int(rz_geom.Ns_plas),:]))

  xn_pest = sp.divide(sp.multiply(xn_pest,sp.multiply(sp.sqrt(jc_pest.G22[:int(rz_geom.Ns_plas),:]), rz_pest.R[:int(rz_geom.Ns_plas),:])), jc_pest.jacobian[:int(rz_geom.Ns_plas),:])*4.0

  expmchi = sp.exp(sp.tensordot(rz_pest.chi,xplas.m, 0)*-1j)

  xmn_pest = sp.dot(sp.dot(xn_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi

  xmn_pest[0,:]=xmn_pest[1,:]
  xmn_pest=sp.array(xmn_pest)


  # Loading qprof to get rational surfaces
  profeq = sp.loadtxt(profeq_path)
  s_pest = profeq[:,0] 
  q_pest = profeq[:,1] 

  # get rational surfaces
  q_max = q_pest.max()
  q_min = q_pest.min()
  m_rational = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
  q_rational = m_rational/float(n)
  s_rational = InterpolatedUnivariateSpline(q_pest, s_pest)(q_rational)

  res = sp.absolute(xmn_pest)
  plt.figure('Displacement Spectrogram, single MARS run')
  plt.contourf(sp.array(xplas.m)[:],rz_geom.s[:rz_pest.Ns_plas], res, 80, cmap=plt.cm.spectral_r)
  plt.plot(m_rational, s_rational,'+k', markersize=6.0, markeredgewidth=1.5)
  plt.colorbar()
  plt.xlabel('m')
  plt.ylabel('s')

  plt.show(block=False)





def plotDispSpect_2coil(xplas_u_path, xplas_l_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, deltaphi): 
  deltaphi_rad = deltaphi*(sp.pi/180.0) 

# below this is same as 1coil version and needs to be modified

  nchi=660

  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  rz_pest = pb.rzcoords(rmzm_pest_path, nchi)
  jc_pest = pb.jacobian(rz_pest)


  xplas_u = pb.xplasma(xplas_u_path, rz_geom, jc_geom)
  xplas_l = pb.xplasma(xplas_l_path, rz_geom, jc_geom)

  xm1_u = sp.copy(xplas_u.xm1)
  xm1_l = sp.copy(xplas_l.xm1)
  
  xm1_total = xm1_u + xm1_l*sp.exp(-1j*deltaphi_rad)


  expmchi = sp.exp(sp.tensordot(xplas_u.m, rz_geom.chi,0)*1j)

  x1_total = sp.dot(xm1_total.T, expmchi)
  # reminder: xn = x1*J/sqrt(G33*G22)
  xn_geom = x1_total*jc_geom.jacobian[:int(rz_geom.Ns_plas),:]/sp.sqrt(jc_geom.G33[:int(rz_geom.Ns_plas),:]*jc_geom.G22[:int(rz_geom.Ns_plas),:])

  xn_pest = scipy.interpolate.griddata((rz_geom.R[:int(rz_geom.Ns_plas),:].ravel(), rz_geom.Z[:int(rz_geom.Ns_plas),:].ravel()), xn_geom.ravel(), (rz_pest.R[:int(rz_geom.Ns_plas),:], rz_pest.Z[:int(rz_geom.Ns_plas),:]))

  xn_pest = sp.divide(sp.multiply(xn_pest,sp.multiply(sp.sqrt(jc_pest.G22[:int(rz_geom.Ns_plas),:]), rz_pest.R[:int(rz_geom.Ns_plas),:])), jc_pest.jacobian[:int(rz_geom.Ns_plas),:])*4.0

  expmchi = sp.exp(sp.tensordot(rz_pest.chi,xplas_u.m, 0)*-1j)

  xmn_pest = sp.dot(sp.dot(xn_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi

  xmn_pest[0,:]=xmn_pest[1,:]
  xmn_pest=sp.array(xmn_pest)

  # Loading qprof to get rational surfaces
  profeq = sp.loadtxt(profeq_path)
  s_pest = profeq[:,0] 
  q_pest = profeq[:,1] 

  # get rational surfaces
  q_max = q_pest.max()
  q_min = q_pest.min()
  m_rational = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
  q_rational = m_rational/float(n)
  s_rational = InterpolatedUnivariateSpline(q_pest, s_pest)(q_rational)

  res = sp.absolute(xmn_pest)
  plt.figure('Displacement Spectrogram, two MARS runs')
  plt.contourf(sp.array(xplas_u.m)[:],rz_geom.s[:rz_pest.Ns_plas], res, 80, cmap=plt.cm.spectral_r)
  plt.plot(m_rational, s_rational,'+k', markersize=6.0, markeredgewidth=1.5)
  plt.colorbar()
  plt.xlabel('m')
  plt.ylabel('s')

  plt.show(block=False)



def plotDispProfiles_1coil(xplas_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n):
  # Not sure this is correct, check against PythonRZPlot for same data
  nchi=660

  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  rz_pest = pb.rzcoords(rmzm_pest_path, nchi)
  jc_pest = pb.jacobian(rz_pest)

  xplas = pb.xplasma(xplas_path, rz_geom, jc_geom)

  xm1 = sp.copy(xplas.xm1)
  expmchi = sp.exp(sp.tensordot(xplas.m, rz_geom.chi,0)*1j)

  x1 = sp.dot(xm1.T, expmchi)
  # reminder: xn = x1*J/sqrt(G33*G22)
  xn_geom = x1*jc_geom.jacobian[:int(rz_geom.Ns_plas),:]/sp.sqrt(jc_geom.G33[:int(rz_geom.Ns_plas),:]*jc_geom.G22[:int(rz_geom.Ns_plas),:])

  xn_pest = scipy.interpolate.griddata((rz_geom.R[:int(rz_geom.Ns_plas),:].ravel(), rz_geom.Z[:int(rz_geom.Ns_plas),:].ravel()), xn_geom.ravel(), (rz_pest.R[:int(rz_geom.Ns_plas),:], rz_pest.Z[:int(rz_geom.Ns_plas),:]))

  xn_pest = sp.divide(sp.multiply(xn_pest,sp.multiply(sp.sqrt(jc_pest.G22[:int(rz_geom.Ns_plas),:]), rz_pest.R[:int(rz_geom.Ns_plas),:])), jc_pest.jacobian[:int(rz_geom.Ns_plas),:])*4.0

  expmchi = sp.exp(sp.tensordot(rz_pest.chi,xplas.m, 0)*-1j)

  xmn_pest = sp.dot(sp.dot(xn_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi

  xmn_pest[0,:]=xmn_pest[1,:]
  xmn_pest=sp.array(xmn_pest)

  # Loading qprof to get rational surfaces
  profeq = sp.loadtxt(profeq_path)
  s_pest = profeq[:,0] 
  q_pest = profeq[:,1] 

  # get rational surfaces
  q_max = q_pest.max()
  q_min = q_pest.min()
  m_rational = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
  q_rational = m_rational/float(n)
  s_rational = InterpolatedUnivariateSpline(q_pest, s_pest)(q_rational)

  res = sp.absolute(xmn_pest)
  plt.figure('Displacement Profiles, single MARS run')
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,30]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,31]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,32]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,33]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,34]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,35]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,36]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,37]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,38]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,39]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,40]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,41]*1e4)
  plt.legend(['m='+x for x in map(str,sp.array(xplas.m)[30:42])],loc=2)

  plt.show(block=False)





def plotDispProfiles_2coil(xplas_u_path, xplas_l_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, deltaphi): 

  print('disp profiles 2 coil')
  deltaphi_rad = deltaphi*(sp.pi/180.0) 

  nchi=660

  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)
  rz_pest = pb.rzcoords(rmzm_pest_path, nchi)
  jc_pest = pb.jacobian(rz_pest)

  xplas_u = pb.xplasma(xplas_u_path, rz_geom, jc_geom)
  xplas_l = pb.xplasma(xplas_l_path, rz_geom, jc_geom)

  xm1_u = sp.copy(xplas_u.xm1)
  xm1_l = sp.copy(xplas_l.xm1)
  
  xm1_total = xm1_u + xm1_l*sp.exp(-1j*deltaphi_rad)
  expmchi = sp.exp(sp.tensordot(xplas_u.m, rz_geom.chi,0)*1j)
  x1_total = sp.dot(xm1_total.T, expmchi)
  # reminder: xn = x1*J/sqrt(G33*G22)
  xn_geom = x1_total*jc_geom.jacobian[:int(rz_geom.Ns_plas),:]/sp.sqrt(jc_geom.G33[:int(rz_geom.Ns_plas),:]*jc_geom.G22[:int(rz_geom.Ns_plas),:])

  xn_pest = scipy.interpolate.griddata((rz_geom.R[:int(rz_geom.Ns_plas),:].ravel(), rz_geom.Z[:int(rz_geom.Ns_plas),:].ravel()), xn_geom.ravel(), (rz_pest.R[:int(rz_geom.Ns_plas),:], rz_pest.Z[:int(rz_geom.Ns_plas),:]))

  xn_pest = sp.divide(sp.multiply(xn_pest,sp.multiply(sp.sqrt(jc_pest.G22[:int(rz_geom.Ns_plas),:]), rz_pest.R[:int(rz_geom.Ns_plas),:])), jc_pest.jacobian[:int(rz_geom.Ns_plas),:])*4.0

  expmchi = sp.exp(sp.tensordot(rz_pest.chi,xplas_u.m, 0)*-1j)

  xmn_pest = sp.dot(sp.dot(xn_pest,expmchi),(rz_pest.chi[1]-rz_pest.chi[0]))/2.0/sp.pi

  xmn_pest[0,:]=xmn_pest[1,:]
  xmn_pest=sp.array(xmn_pest)

  # Loading qprof to get rational surfaces
  profeq = sp.loadtxt(profeq_path)
  s_pest = profeq[:,0] 
  q_pest = profeq[:,1] 

  # get rational surfaces
  q_max = q_pest.max()
  q_min = q_pest.min()
  m_rational = sp.arange(int(q_min*n)+1, int(q_max*n)+1)
  q_rational = m_rational/float(n)
  s_rational = InterpolatedUnivariateSpline(q_pest, s_pest)(q_rational)

  res = sp.absolute(xmn_pest)
  plt.figure('Displacement Profiles, two MARS runs')
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,30]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,31]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,32]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,33]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,34]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,35]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,36]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,37]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,38]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,39]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,40]*1e4)
  plt.plot(rz_geom.s[:rz_pest.Ns_plas], res[:,41]*1e4)
  plt.legend(['m='+x for x in map(str,sp.array(xplas_u.m)[30:42])],loc=2)

  plt.show(block=False)


def plotXplasRZ_1coil(xplas_path, rmzm_geom_path, rmzm_pest_path, profeq_path, n, r0):
  nchi=660
  rz_geom = pb.rzcoords(rmzm_geom_path, nchi)
  jc_geom = pb.jacobian(rz_geom)

  xplas_geom = pb.xplasma(xplas_path, rz_geom, jc_geom)

  XN = xplas_geom.xn*r0

  R = rz_geom.R[:int(rz_geom.Ns_plas),:]*r0
  Z = rz_geom.Z[:int(rz_geom.Ns_plas),:]*r0

  Rmin = R.min() - 0.2
  Rmax = R.max() + 0.2
  Zmin = Z.min() - 0.2
  Zmax = Z.max() + 0.2

  plt.figure('Displacement(R,Z), single MARS run', figsize = (8,5))
  plt.subplot(131)
  plt.contourf(R,Z,sp.absolute(XN)*1e3,100, cmap = plt.cm.afmhot)#, vmax = absMax, levels = sp.linspace(0,absMax,100))
  plt.title('Abs{\\xi_n} (mm)')
  plt.colorbar(format = '%0.1f')
  plt.xlim([Rmin,Rmax])
  plt.ylim([Zmin, Zmax])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(132)
  plt.contourf(R,Z,XN.real*1e3,100, cmap = plt.cm.afmhot)#, vmax = absMax, levels = sp.linspace(xnReMin,xnReMax,100))
  plt.title('Re{\\xi_n} (mm)')
  plt.colorbar(format = '%0.1f')
  plt.xlim([Rmin,Rmax])
  plt.ylim([Zmin, Zmax])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.subplot(133)
  plt.contourf(R,Z,XN.imag*1e3,100, cmap = plt.cm.afmhot)#, vmax = absMax, levels = sp.linspace(xnImMin,xnImMax,100))
  plt.title('Im{\\xi_n} (mm)')
  plt.colorbar(format = '%0.1f')
  plt.xlim([Rmin,Rmax])
  plt.ylim([Zmin, Zmax])
  plt.gca().set_aspect('equal', adjustable='box')

  plt.tight_layout()
  plt.show(block=False)

















